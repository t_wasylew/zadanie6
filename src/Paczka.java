public class Paczka {

    private String sender;
    private String recipient;
    private boolean send;
    private boolean content;

    public Paczka() {
    }

    public Paczka(String recipient, boolean content) {
        this.recipient = recipient;
        this.content = content;
        setSend(false);
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public boolean isSend() {
        return send;
    }

    public void setSend(boolean send) {
        this.send = send;
    }

    public boolean isContent() {
        return content;
    }

    public void setContent(boolean content) {
        this.content = content;
    }

    public void send() {
        if (!isSend() && recipient != null && isContent()) {
            System.out.println("Sending pack.");
            setSend(true);
        } else if (isSend()) {
            System.out.println("Already sent");
        } else {
            System.out.println("Something wrong with the pack. No recipient or content");
        }
    }

    public void prioritySend() {
        if (!isSend()) {
            if (recipient != null) {
                send();
            } else {
                System.out.println("Something wrong with the pack. No recipient or content");
            }
        }
    }
}
